#!/usr/bin/python
# -*- coding: utf-8 -*-
import facebook
from sqlalchemy import *
import random
import requests

DB_CONN = 'sqlite:///quotes.db'
QUOTES_TABLE = 'quotes_table'
SOCMIXO_APPID = '262532657428620'
SOCMIXO_APP_SECRET = 'f81b3c4552bf4ee4f351dfa2ec35bac2'
SOCMIXO_PAGE_ID = '293062060837532'
# for a new token goto https://developers.facebook.com/tools/debug/accesstoken/
SOCMIXO_PAGE_TOKEN='EAADuxaXJuIwBAKRPjCb9ROq4pSB7ZAWxi5OVrSXZCKTxbd04eyS54NkS4tGaIinJblm7mcniXhonxEZAvaXV2M89lxZCy8jDNk0dEpNVirJOgdLd5MiqbAyy4tM7FBJip0rtd7VkgP9d1lRyXK4X2a0Hmz71vkwZD'

#SOCMIXO_PAGE_TOKEN='EAADuxaXJuIwBADVIU0oI3z0LISBYqqNsZCZBpS4m46MVcnEKGZBHmGM5mZCOjIsnzCHrcqskR882lvjtRR43DAVK7YhYuTS3S6FoHefR0LZA7BA21PnPt6YwYxj345s5SsZBw4ShbAFyexFRu8hyYl'


def connect_db():
    db_engine = create_engine(DB_CONN)
    connections = db_engine.connect()
    metadata = MetaData(connections)
    if db_engine.dialect.has_table(connections, QUOTES_TABLE):
        quotes_table = Table(QUOTES_TABLE, metadata, autoload=True)
    else:
        quotes_table = Table(QUOTES_TABLE, metadata,
                             Column('id', BigInteger().with_variant(Integer, "sqlite"), primary_key=True),
                             Column('url', String(260)),
                             Column('quote', String(300)),
                             Column('author', String(100))
                             )
        quotes_table.create()
    return quotes_table, connections


# ref: https://developers.facebook.com/tools/debug/accesstoken
def get_faceplant_access_api(app_id, app_secret):
    cfg = {
        "page_id": SOCMIXO_PAGE_ID,  # Step 1
        "access_token": SOCMIXO_PAGE_TOKEN  # Step 3
    }
    api = get_api(cfg)
    return api


def get_api(cfg):
  graph = facebook.GraphAPI(cfg['access_token'])
  # Get page token to post as the page. You can skip
  # the following if you want to post as yourself.
  resp = graph.get_object('me/accounts')
  page_access_token = None
  for page in resp['data']:
    if page['id'] == cfg['page_id']:
      page_access_token = page['access_token']
  graph = facebook.GraphAPI(page_access_token)
  return graph
  # You can also skip the above if you get a page token:
  # http://stackoverflow.com/questions/8231877/facebook-access-token-for-pages
  # and make that long-lived token as in Step 3


def write_post():
    quotes_table, conn = connect_db()
    api = get_faceplant_access_api(SOCMIXO_APPID, SOCMIXO_APP_SECRET)

    # select a random post
    qry = quotes_table.select()
    myquotes = conn.execute(qry)
    quotes = []
    for row in myquotes.fetchall():
       quotes.append(row)
    irnd = int(random.random()* len(quotes))

    msg_post = "{}\n\n{}".format(quotes[irnd][2],quotes[irnd][3])
    api.put_wall_post(msg_post)
    print("Just posted the message: %s" % msg_post)


def main():
    write_post()



main()
