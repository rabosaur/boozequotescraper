# encoding=utf8
import re
import sys
import urllib2
from bs4 import BeautifulSoup as soup
from sqlalchemy import *
import csv

CSV_FILE = "quotes.csv"

reload(sys)
# noinspection PyUnresolvedReferences
sys.setdefaultencoding('Cp1252')

# DB_CONN = 'mysql://sql3122144:TQGHiJb8eK@sql3.freemysqlhosting.net/sql3122144'
DB_CONN = 'sqlite:///quotes.db'
QUOTES_TABLE = 'quotes_table'
header = '"quote","author","url"'


def clean(s):
    s = s.replace('-', '')
    s = s.replace('\n', '')
    s = s.replace('\"', '')
    s = s.replace('&nbsp', ' ')
    while '  ' in s:
        s = s.replace('  ', ' ')
    while '; ;' in s:
        s = s.replace('; ;', ';;')
    while ';;' in s:
        s = s.replace(';;', '')
    s = s.strip()
    while s.endswith(';'):
        s = s[0:-1]
    while s.startswith(';'):
        s = s[1:]
    return s.strip().decode('utf-8', 'ignore')


def output_tuple(writer, quote, author, url):
    new_quote = writer.insert()
    quote = re.sub(r'[^\x00-\x7F]+', ' ', quote).strip()
    author = re.sub(r'[^\x00-\x7F]+', ' ', author).strip()
    new_quote.execute(url=url, quote=quote, author=author)
    with open(CSV_FILE, "ab") as f:
        f.writelines('"{}","{}","{}"\n'.format(str(quote), str(author), str(url)))


def cocktail_movie(writer):
    url = "http://www.quotes.net/movies/2285"
    print "Processing {}".format(url)
    opener = urllib2.build_opener()
    opener.addheaders = [('User-Agent', 'Mozilla/5.0')]
    page = opener.open(url)
    data = soup(unicode(page.read()), "html.parser")
    quotes = data.find_all("div", {'class': 'disp-mquote-ext clearfix'})
    for quote in quotes:
        joke = quote.text
        author = "Cocktail[1995]"
        output_tuple(writer, joke, author, url)
    pass


def quote_garden(writer):
    url = "http://www.quotegarden.com/alcohol.html"
    print "Processing {}".format(url)
    opener = urllib2.build_opener()
    opener.addheaders = [('User-Agent', 'Mozilla/5.0')]
    page = opener.open(url)
    data = soup(unicode(page.read()), "html.parser")
    quotes = [x for x in data.find_all("p") if "left" in x['align']][0].text.splitlines()
    quotes = [x for x in quotes if len(x) > 1]
    for quote in quotes:
        parts = quote.split("~")
        # noinspection PyBroadException
        try:
            author = parts[1].replace('"', "'")
            joke = parts[0]
            output_tuple(writer, joke, author, url)
        except:
            break
    pass


def brainy_quote(writer):
    url_template = "http://www.brainyquote.com/quotes/keywords/cocktail{}.html"
    for page_number in range(0, 8):
        if page_number == 0:
            url = url_template.format("")
        else:
            url = url_template.format("_{}".format(page_number))
        print "Processing {}".format(url)
        opener = urllib2.build_opener()
        opener.addheaders = [('User-Agent', 'Mozilla/5.0')]
        page = opener.open(url)
        data = soup(unicode(page.read()), "html.parser")
        quotes = [x.text for x in data.find_all("span", {'class': 'bqQuoteLink'})]
        authors = [x.text for x in data.find_all("div", {'class': 'bq-aut'})]
        for idx in range(0, len(authors)):
            output_tuple(writer, quotes[idx], authors[idx], url)
        # should we stop?
        lis = [x.text for x in data.find_all("li", {'class': 'disabled'}) if len(x.find_all("span")) > 0]
        if len(lis) > 0:
            break
    pass


def cool_n_smart(writer):
    url = 'http://www.coolnsmart.com/alcohol_sayings/'
    print "Processing {}".format(url)
    page = urllib2.urlopen(url)
    data = soup(unicode(page.read()), "html.parser")
    # print (data.prettify())
    index = data.find_all("p")
    for joke in index:
        # noinspection PyBroadException
        try:
            joke_text = joke.getText()
            if "Copyright" in joke_text:
                continue
            output_tuple(writer, joke_text, "", url)
        except:
            pass
    pass


def individual_quotes(writer):
    print "Processing individual quotes"
    with open("individualquotes.txt", "rb") as f:
        quoterdr = csv.reader(f)
        for row in quoterdr:
            author = row[1]
            quote = row[0]
            output_tuple(writer, quote.strip(), author, 'local')


def good_reads(writer):
    for page_count in range(1, 100):
        url = "https://www.goodreads.com/quotes/tag/drinking?page={page}".format(page=page_count)
        print "Processing {}".format(url)
        page = urllib2.urlopen(url)
        data = soup(page.read(), "html.parser")
        # print (soup.prettify())
        divs = data.find_all('div')
        divs = [x for x in divs if "class" in x.attrs]
        divs = [x for x in divs if "quoteText" in x['class']]
        if divs:
            divs = [x for x in divs if "CDATA[" not in x.get_text()]
            for div in divs:
                authors = div.find_all('a')
                author = unicode(authors[0].get_text())
                quote = unicode(div.get_text().replace(author, ""))
                output_tuple(writer, quote.strip(), author, url)
        else:
            break


def quotes_drink(writer):
    url = "http://www.gdargaud.net/Humor/QuotesDrink.html"
    print "Processing {}".format(url)

    page = urllib2.urlopen(url)
    booze2 = soup(page.read(), "html.parser")
    # print soup.prettify(booze2)
    quotes = booze2.findAll('blockquote')
    quotes = [x for x in quotes if len(x.text) > 10]
    for quote in quotes:
        q = a = ''
        s2 = soup(quote.text, "html.parser")
        parts = s2.text.split('&mdash')
        if len(parts) is 3:
            q = clean(parts[0] + parts[1])
            a = clean(parts[2])
        if len(parts) is 2:
            q = clean(parts[0])
            a = clean(parts[-1])
        if len(parts) is 1:
            q = clean(parts[0])
            a = ""
        output_tuple(writer, q, a, url)
    pass


def drinking_quotes(writer):
    url = "http://www.worldwidedrinks.com/DrinkingQuotes.html"
    print "Processing {}".format(url)
    page = urllib2.urlopen(url)
    booze2 = soup(page.read(), "html.parser")

    # print soup.prettify(booze2)
    quotes = booze2.findAll('p')
    quotes = [x for x in quotes if len(x.text) > 10]
    for quote in quotes:
        s2 = soup(quote.text, "html.parser")
        parts = s2.text.split('\n')
        if len(parts) is 3:
            q = clean(parts[0] + parts[1])
            a = clean(parts[2].replace('-', ''))
            output_tuple(writer, q, a, url)
    pass


def best_50(writer):
    url = 'http://www.thefiftybest.com/useful_info/best_drinking_quotes/'
    print "Processing {}".format(url)
    page = urllib2.urlopen(url)
    data = soup(unicode(page.read()), "html.parser")
    # print (data.prettify())
    index = data.find_all("li")
    index = [x.text for x in index]
    for joke in index:
        # noinspection PyBroadException
        try:
            if len(joke.splitlines()) == 1 and joke.strip() > 5:
                parts = joke.split('~')
                output_tuple(writer, parts[0], parts[1], url)
        except:
            pass
    pass


def just_one_liners(quotes_table):
    quotes = []
    for pagenum in range(1, 14):
        url = 'http://www.just-one-liners.com/category/food-drink/alcohol-food/page/{}'.format(pagenum)
        print "Processing {}".format(url)
        page = urllib2.urlopen(url)
        data = soup(page.read(), "html.parser")
        divs = data.find_all("div")
        divs = [x for x in divs if 'class' in x.attrs]
        divs = [x for x in divs if 'post' in x.attrs['class']]
        for div in divs:
            # process a quote
            try:
                quote = div.find_all("h2")[0].text
                author = div.find_all("div", {'class': 'quoteauthor'})[0].text
                quotes.append((author, quote, url))
            except Exception as e:
                print "Exception processing quote on {}\n{}".format(url, e)
                continue
    quotes = list(set(quotes))
    for quote in quotes:
        output_tuple(quotes_table, quote[1], quote[0], quote[2])
    pass


def thought_catalog(writer):
    url = r'http://thoughtcatalog.com/lorenzo-jensen-iii/2015/05/lets-toast-to-these-70-classic-quotes-about-drinking/'
    print "Processing {}".format(url)
    page = urllib2.urlopen(url)
    data = soup(unicode(page.read()), "html.parser")
    quotes = data.find_all('div')
    divs = [x for x in quotes if "class" in x.attrs]
    divs = [x for x in divs if "tc_intro" in x.attrs['class'][0]]
    for div in divs:
        (quote, author) = div.text.splitlines()
        author = clean(author)[1:]
        output_tuple(writer, quote, author, url)
    pass


def connect_db():
    db_engine = create_engine(DB_CONN)
    connections = db_engine.connect()
    metadata = MetaData(connections)
    if db_engine.dialect.has_table(connections, QUOTES_TABLE):
        quotes_table = Table(QUOTES_TABLE, metadata, autoload=True)
    else:
        quotes_table = Table(QUOTES_TABLE, metadata,
                             Column('id', BigInteger().with_variant(Integer, "sqlite"), primary_key=True),
                             Column('url', String(260)),
                             Column('quote', String(300)),
                             Column('author', String(100)),
                             Column('used', Boolean)
                             )
        quotes_table.create()
    return quotes_table


def main():
    quotes_table = connect_db()
    with open(CSV_FILE, "ab") as f:
        f.writelines(header)
    thought_catalog(quotes_table)
    individual_quotes(quotes_table)
    best_50(quotes_table)
    cocktail_movie(quotes_table)
    quote_garden(quotes_table)
    brainy_quote(quotes_table)
    drinking_quotes(quotes_table)
    quotes_drink(quotes_table)
    good_reads(quotes_table)
    cool_n_smart(quotes_table)
    just_one_liners(quotes_table)


main()
